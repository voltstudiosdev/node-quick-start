var express = require("express");
var path = require("path");
var app = express();
var port = 8080;

var publicPath = path.join(__dirname, '/public');

app.use(express.static(publicPath));

app.get("/", function(req, res) {
	res.sendFile(publicPath + '/index.html');
});

app.listen(port);
console.log('App running on port: ' + port);